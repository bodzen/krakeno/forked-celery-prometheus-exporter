FROM python:3.8.6-alpine3.12

MAINTAINER dh4rm4

COPY requirements.txt /

RUN pip install -r /requirements.txt \
	&& mkdir -p /app/

COPY celery-prometheus-exporter/ /app/

USER 1000:1000
WORKDIR /app
ENV PYTHONUNBUFFERED=1

ENTRYPOINT ["python","/app/celery_prometheus_exporter.py"]
