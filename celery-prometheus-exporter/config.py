#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os


try:
    SENTRY_URI = os.environ['SENTRY_URI']
except KeyError:
    SENTRY_URI = None

QUEUE_LENGTH_PERIOD = 5
DEFAULT_BROKER = os.environ.get('BROKER_URL', 'redis://172.18.0.2:6379/0')
DEFAULT_ADDR = os.environ.get('DEFAULT_ADDR', '0.0.0.0:9540')
