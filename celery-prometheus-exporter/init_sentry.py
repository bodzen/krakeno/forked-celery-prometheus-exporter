#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sentry_sdk

from config import SENTRY_URI


def init_sentry():
    if SENTRY_URI:
        sentry_sdk.init(
            SENTRY_URI,
            traces_sample_rate=1.0
        )
        print('[+] Connection to Sentry\'s servers have been initialized')
    else:
        print('[-] Sentry\'s configuration file not found. No connection established.')
